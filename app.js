const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const fileUpload = require('express-fileupload')
const methodOverride = require('method-override')
const streamifier = require('streamifier')
const slug = require('slug')
const path = require('path')

const app = express()

// Middleware
app.use(bodyParser.json())
app.use(fileUpload())
app.use(methodOverride('_method'))
app.set('view engine', 'ejs')

// Mongodb URI

const mongoURI = 'mongodb://localhost:27017/media'

// // Create mongo connection

let bucket

mongoose.connect(mongoURI, { useNewUrlParser: true }, function(error, client) {
  if (error) {
    console.log('MongoDB Connection Error. Please make sure that MongoDB is running.')
    process.exit(1)
  }

  // create bucket

  bucket = new mongoose.mongo.GridFSBucket(client.db, {
    bucketName: 'fs'
  })
})

// @route GET /
// @desc Loads form

app.get('/', (req, res) => {
  res.render('index')
})

// Route POST /
// @desc Upload file to DB

app.post('/upload', (req, res) => {
  let { name, mimetype: contentType } = req.files.file
  const filename = slug(name.split(path.extname(name))[0], '-') + path.extname(name)

  let readableTrackStream = streamifier.createReadStream(req.files.file.data)

  let uploadStream = bucket.openUploadStream(filename, { contentType })
  let id = uploadStream.id
  readableTrackStream.pipe(uploadStream)

  uploadStream.on('error', () => {
    return res.status(500).json({ message: 'Error uploading file' })
  })

  uploadStream.on('finish', () => {
    return res.status(200).json({
      message: 'File uploaded successfully, stored under Mongo ObjectID: ' + id,
      href: req.protocol + '://' + req.get('host') + `/files/` + filename
    })
  })
})

// Route GET /files/:filename
// @decs Display file in JSON

app.get('/files/:filename', (req, res) => {
  const filename = req.params.filename
  res.set('Accept-Ranges', 'bytes')
  bucket.find({ filename }).toArray((err, files) => {
    if (err) res.sendStatus(404)
    let file = files[0]
    res.set('Content-Type', file.contentType)
    res.set('Content-Length', file.length)
    const downloadStream = bucket.openDownloadStreamByName(file.filename)
    downloadStream.on('data', chunk => {
      res.write(chunk)
    })

    downloadStream.on('error', () => {
      res.sendStatus(404)
    })

    downloadStream.on('end', () => {
      res.end()
    })
  })
})

const port = 5000

app.listen(port, () => console.log(`Server start on port: ${port}`))
